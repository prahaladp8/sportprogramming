import java.util.*;
import IO.*;


import java.util.*;
import java.lang.*;
import java.io.*;

class hcanon{
	static double time[][];
	static int walkSpeed = 5; //m/s
	static double canonPropel = 50.0; //m
	static int canonDelay = 2; //second
	
	
	static double getDistance(double x1,double y1,double x2,double y2){
		return Math.sqrt(((x2-x1)*(x2-x1)) + ((y2-y2)*(y2-y1)));
	}
	
	static void preprocess(double[] canPosX, double[] canPosY , double[][] time,int n){
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
//				get dist via walk	
				double distance  = getDistance(canPosX[i], canPosY[i], canPosX[j], canPosY[j]) ; 
				double walkTime = distance / walkSpeed;
				double canonPropelTimeNet = 0.0;
				if(distance <= canonPropel)
					canonPropelTimeNet = canonDelay;
				else
					canonPropelTimeNet = canonDelay + ((distance - canonPropel)/walkSpeed) ;
				
				time[i][j]= Math.min(walkTime, canonPropelTimeNet);
				time[j][i] = time[i][j];				
			}
		}
	}
	
	static double getMinTime(double x1,double y1,double x2,double y2){
		double distance  = getDistance(x1,y1,x2,y2) ; 
		double walkTime = distance / walkSpeed;
		double canonPropelTimeNet = 0.0;
		if(distance <= canonPropel)
			canonPropelTimeNet = canonDelay;
		else
			canonPropelTimeNet = canonDelay + ((distance - canonPropel)/walkSpeed) ;
		
		return Math.min(walkTime, canonPropelTimeNet);
	}
	
	static void preprocess2(int s , int e , double[][] time){
		for (int i = 1; i < time.length-1; i++) {
			for (int j = 1; j < time.length-1; j++) {
				
			}
		}
	}
	
	//static double ans = 0;
	
	static double getDistanceFin(double[][]time,int start , int end,double ans){
		if(start == end)
            return 0;
        if(start < end){
            double temp = Double.MAX_VALUE;
			for (int i = start+1; i <= time[start].length-2; i++) {
				temp = Math.min(temp,Math.min( time[start][end],time[start][i]+getDistanceFin(time, i, end,ans))); 	
			}
            ans += temp;
		}else{
			return 0;
		}
        System.out.println("start : "+start+" end "+end+" Min dist = "+ans);
		return ans;
	}
	
	static void printTime(double[][] time,int n){
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(time[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	public static void  main(String args[])throws IOException {
		double sx,sy;
		double ex,ey;
		int n;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String temp[] = br.readLine().split(" ");
		sx = Double.parseDouble(temp[0]);
		sy = Double.parseDouble(temp[1]);		
		temp = br.readLine().split(" ");
		n = Integer.parseInt(br.readLine());
		ex = Double.parseDouble(temp[0]);
		ey = Double.parseDouble(temp[1]);
		time = new double[n+2][n+2];
		double canPosX [] = new double[n+2];
		double canPosY [] = new double[n+2];
		for (int i = 1; i < canPosX.length-1; i++) {
			temp = br.readLine().split(" ");
			canPosX[i] = Double.parseDouble(temp[0]);
			canPosY[i] = Double.parseDouble(temp[1]);
		}
		canPosX[0] = sx;
		canPosX[n+1] = ex;
		canPosY[0] = sy;
		canPosY[n+1] = ey;
		preprocess(canPosX , canPosY, time,n+2);
		
//		get canon closest to start and
//		;
		printTime(time, n+2);
        double ans =0;
		System.out.println(getDistanceFin(time, 0, n+1,ans));
	}
}