package Codechef;

import java.util.*;
import java.io.*;

//import InputReader.SpaceCharFilter;

class InputReader {
    private final InputStream stream;
    private final byte buf[] = new byte[1024];
    private int curChar;
    private int numChars;
    private SpaceCharFilter filter;
 
    public InputReader(InputStream stream) {
        this.stream = stream;
    }
 
    public int read() {
        if(numChars == -1) throw new InputMismatchException();
        if(curChar >= numChars) {
            curChar = 0;
            try {
                numChars = stream.read(buf);
            } catch(IOException e) {
                throw new InputMismatchException();
            }
            if(numChars <= 0) return -1;
        }
        return buf[curChar++];
    }
 
    public int readInt() {
        int c = read();
        while(isSpaceChar(c)) c = read();
        int sgn = 1;
        if(c == '-') {
            sgn = -1;
            c = read();
        }
        int res = 0;
        do {
            if(c < '0' || c > '9') throw new InputMismatchException();
            res *= 10;
            res += c - '0';
            c = read();
        } while(!isSpaceChar(c));
        return res*sgn;
    }
 
    public long readLong() {
        int c = read();
        while(isSpaceChar(c)) c = read();
        int sgn = 1;
        if(c == '-') {
            sgn = -1;
            c = read();
        }
        long res = 0;
        do {
            if(c < '0' || c > '9') throw new InputMismatchException();
            res *= 10;
            res += c - '0';
            c = read();
        } while(!isSpaceChar(c));
        return res*sgn;
    }
 
    public char readChar() {
        int c = read();
        while(isSpaceChar(c)) c = read();
        return (char) c;
    }
 
    public String readString() {
        int c = read();
        while(isSpaceChar(c)) c = read();
        StringBuilder res = new StringBuilder();
        do {
            res.appendCodePoint(c);
            c = read();
        } while(!isSpaceChar(c));
        return res.toString();
    }
 
    public String readLine() {
        int c = read();
        while(isNewLineChar(c)) c = read();
        StringBuilder res = new StringBuilder();
        do {
            if(c<0) c += 256;
            res.appendCodePoint(c);
            c = read();
        } while(!isNewLineChar(c));
        return res.toString();
    }
 
    public String next() {
        return readString();
    }
 
    public boolean isSpaceChar(int c) {
        if(filter != null) return filter.isSpaceChar(c);
        return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
    }
 
    public boolean isNewLineChar(int c) {
        if(filter != null) return filter.isNewLineChar(c);
        return c == '\n' || c == '\r' || c == -1;
    }
 
    public interface SpaceCharFilter {
        public boolean isSpaceChar(int ch);
        public boolean isNewLineChar(int ch);
    }
}
 
class OutputWriter {
    private final PrintWriter writer;
 
    public OutputWriter(OutputStream stream) {
        this.writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(stream)));
    }
 
    public OutputWriter(Writer writer) {
        this.writer = new PrintWriter(writer);
    }
 
    public void print(Object...objects) {
        for(int i=0; i<objects.length; i++) {
            if(i != 0) writer.print(' ');
            writer.print(objects[i]);
        }
    }
 
    public void println(Object...objects) {
        print(objects);
        writer.println();
    }
 
    public void flush() {
        writer.flush();
    }
 
    public void close() {
        writer.close();
    }
}

public class adtri {
	
	public static void  main(String args[]) {
		InputReader in = new InputReader(System.in);
		OutputWriter out =  new OutputWriter(System.out);
		
		int t = in.readInt();
		
		while(t-- > 0){
			int n = in.read();
		}
	}
}
