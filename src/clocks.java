	import java.util.*;
	import java.lang.*;
	import java.io.*;
	 
	class InputReader {
	    private final InputStream stream;
	    private final byte buf[] = new byte[1024];
	    private int curChar;
	    private int numChars;
	    private SpaceCharFilter filter;
	 
	    public InputReader(InputStream stream) {
	        this.stream = stream;
	    }
	 
	    public int read() {
	        if(numChars == -1) throw new InputMismatchException();
	        if(curChar >= numChars) {
	            curChar = 0;
	            try {
	                numChars = stream.read(buf);
	            } catch(IOException e) {
	                throw new InputMismatchException();
	            }
	            if(numChars <= 0) return -1;
	        }
	        return buf[curChar++];
	    }
	 
	    public int readInt() {
	        int c = read();
	        while(isSpaceChar(c)) c = read();
	        int sgn = 1;
	        if(c == '-') {
	            sgn = -1;
	            c = read();
	        }
	        int res = 0;
	        do {
	            if(c < '0' || c > '9') throw new InputMismatchException();
	            res *= 10;
	            res += c - '0';
	            c = read();
	        } while(!isSpaceChar(c));
	        return res*sgn;
	    }
	 
	    public long readLong() {
	        int c = read();
	        while(isSpaceChar(c)) c = read();
	        int sgn = 1;
	        if(c == '-') {
	            sgn = -1;
	            c = read();
	        }
	        long res = 0;
	        do {
	            if(c < '0' || c > '9') throw new InputMismatchException();
	            res *= 10;
	            res += c - '0';
	            c = read();
	        } while(!isSpaceChar(c));
	        return res*sgn;
	    }
	 
	    public char readChar() {
	        int c = read();
	        while(isSpaceChar(c)) c = read();
	        return (char) c;
	    }
	 
	    public String readString() {
	        int c = read();
	        while(isSpaceChar(c)) c = read();
	        StringBuilder res = new StringBuilder();
	        do {
	            res.appendCodePoint(c);
	            c = read();
	        } while(!isSpaceChar(c));
	        return res.toString();
	    }
	 
	    public String readLine() {
	        int c = read();
	        while(isNewLineChar(c)) c = read();
	        StringBuilder res = new StringBuilder();
	        do {
	            if(c<0) c += 256;
	            res.appendCodePoint(c);
	            c = read();
	        } while(!isNewLineChar(c));
	        return res.toString();
	    }
	 
	    public String next() {
	        return readString();
	    }
	 
	    public boolean isSpaceChar(int c) {
	        if(filter != null) return filter.isSpaceChar(c);
	        return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
	    }
	 
	    public boolean isNewLineChar(int c) {
	        if(filter != null) return filter.isNewLineChar(c);
	        return c == '\n' || c == '\r' || c == -1;
	    }
	 
	    public interface SpaceCharFilter {
	        public boolean isSpaceChar(int ch);
	        public boolean isNewLineChar(int ch);
	    }
	}
	 
	class OutputWriter {
	    private final PrintWriter writer;
	 
	    public OutputWriter(OutputStream stream) {
	        this.writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(stream)));
	    }
	 
	    public OutputWriter(Writer writer) {
	        this.writer = new PrintWriter(writer);
	    }
	 
	    public void print(Object...objects) {
	        for(int i=0; i<objects.length; i++) {
	            if(i != 0) writer.print(' ');
	            writer.print(objects[i]);
	        }
	    }
	 
	    public void println(Object...objects) {
	        print(objects);
	        writer.println();
	    }
	 
	    public void flush() {
	        writer.flush();
	    }
	 
	    public void close() {
	        writer.close();
	    }
	}
	 
	class clocks{
	 
	 
		static double hoursSpeed = 0.5;
		static double minsSpeed = 6;
	 
		public static boolean permittted(double x,double y,double a)
		{
			double temp = getAngle(x,y); 
			if((temp-a) <= (1/120.0) && (temp-a)>=((-1/120.0)))
				return true;
			else
				return false;
		}
	 
		public static double getAngle(double hours,double mins){
			double mini = Math.min(hours,mins);
			double maxi = Math.max(hours,mins);
			double temp = maxi - mini;
			return Math.min((360-temp),temp);
		}
	 
		public static String printTime(int hour,int min){
			String a1,a2;
			if(hour<10)
				a1="0"+hour+":";
			else
				a1 = hour+":";
			if(min<10)
				a2 = "0"+min+"";
				// out.print("0"+min);
			else
				a2 = min+"";
				// out.print(min);
			a1 = a1+a2;
			// System.out.println(a1);
			return a1;		
		}
	 
		public static double getLag(int hour){
			return (hour*30.0);
		}
	 
		// public static  double lagCover(int lag){
			// time taken to cover initial lag by mins hand
			// int lagMin = lag/minsSpeed;
			// distance travelled by hours hand meanwhile
			// double 
	 
		// }
	 
	 
		public static void main(String[] args)throws IOException {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			// InputReader in = new InputReader(System.in);
			OutputWriter out = new OutputWriter(System.out);
			int t = Integer.parseInt(br.readLine());
			while (t-- > 0){
				double a = Double.parseDouble(br.readLine());
				int hour = 0;
				while(hour<12){
					double lag = getLag(hour);
					double hoursLocation = lag;
					double minutesLocation=0;
					if(hour<=6)
						minutesLocation = 0;
					else{
						minutesLocation = 360;
					}
					boolean changed = false; 
					for (int minutes=0;minutes<=59;minutes++) {
						if(permittted(hoursLocation,minutesLocation,a))
							out.println(printTime(hour,minutes));
						hoursLocation+=hoursSpeed;
						if(hour<=6){
							minutesLocation+=minsSpeed;
	 
						}else{
							if(minutes>=30 && !changed){
								minutesLocation-=360;
								changed = true;
							}
							minutesLocation+=minsSpeed;
						}
					}
					hour++;
				}
			}
			out.flush();
	        out.close();
		}
	} 